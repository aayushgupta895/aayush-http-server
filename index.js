const http = require("http");
const fs = require("fs");
const path = require("path");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((req, res) => {
  if (req.url == "/html") {
    console.log("req.url", req.url);
    fs.readFile(path.join(__dirname, "index.html"), (err, content) => {
      if (err) {
        if (err.code == "ENOENT") {
          res.writeHead(404);
          res.end({
            message: "Not found",
          });
        } else {
          res.writeHead(500);
          res.end({
            message: "internal server error",
          });
        }
      } else {
        res.writeHead(200, {
          "Content-Type": "text/html",
        });
        res.end(content);
      }
    });
  }
  if (req.url == "/json") {
    fs.readFile(path.join(__dirname, "index.json"), (err, content) => {
      if (err) {
        if (err.code == "ENOENT") {
          res.writeHead(404);
          res.end({
            message: "Not found",
          });
        } else {
          res.writeHead(500);
          res.end({
            message: "internal server error",
          });
        }
      } else {
        res.writeHead(200, {
          "Content-Type": "application/json",
        });
        res.end(content);
      }
    });
  }
  if (req.url == "/uuid") {
    const uuid = uuidv4();
    res.end(
      JSON.stringify({
        uuid: uuid,
      })
    );
  }
  if (req.url.startsWith("/status/")) {
    const path = req.url.split("/");
    const statusCode = +path[2];
    if (
      typeof statusCode !== "number" ||
      isNaN(statusCode) ||
      path.length > 3 ||
      statusCode == 404 ||
      statusCode > 599 ||
      statusCode < 100
    ) {
      res.end(`404 : not found`);
    } else {
      console.log("inside");
      res.writeHead(statusCode);
      let message = "";
      switch (Math.floor(statusCode / 100)) {
        case 1:
          message = "informational";
          break;
        case 2:
          message = "success";
          break;
        case 3:
          message = "redirection";
          break;
        case 4:
          message = "client error";
          break;
        case 5:
          message = "server error";
          break;
      }
      console.log(`here also`);
      res.end(
        JSON.stringify({
          statusCode: statusCode,
          message: message,
        })
      );
    }
  }
  if (req.url.startsWith("/delay/")) {
    const path = req.url.split("/");
    const delay = +path[2];
    if (typeof delay !== "number" || isNaN(delay)) {
      res.writeHead(404);
      res.end(`404 : not found`);
    } else {
      setTimeout(() => {
        res.writeHead(200);
        res.end(`after ${delay} seconds`);
      }, +delay * 1000);
    }
  }
});

server.listen(2000, () => {
  console.log(`listening on port 2000`);
});
